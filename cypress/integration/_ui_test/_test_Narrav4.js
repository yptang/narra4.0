/// <reference types="Cypress" />

import { host4, token, lang, langc, sportId, marketId, includeEventGroup, eventId, windowWidth, windowHeight } from './constants'


  
//Narra4.0
  describe('Narra4.0', function () {
    let FastBetKey3
    let OddsType_Sev
    let arrPeriod_Sev
    let arrPeriod_Sev1
    let arrBetType_Sev
    let arrBetTypeSelection_Sev
    let arrOdds_Sev

    beforeEach(function () {
        cy.viewport('iphone-6+')
        cy.visit(`${host4}?token=${token}&languageCode=${lang}`)
        cy.on('uncaught:exception', (err, runnable) => {
            return false
        })

        cy.contains('My Account').click()
        cy.wait(200)
        cy.contains('Preferences').click()

        
    //     cy.get('#menu_slider > div.menu_item_list > a:nth-child(2) > div').then(($Preferences) => {
    //       cy.wrap($Preferences).click()
    //   })



        cy.get(':nth-child(10) > .preferences_selections > :nth-child(3) > .normalInput').then(($FastBetKey3) => {
            FastBetKey3 = getFastBetKey3($FastBetKey3)
        })
    })

    beforeEach(function () {
        cy.viewport(windowWidth, windowHeight)
        cy.visit(`${host}/sev/${sportId}/${marketId}/${eventId}/${includeEventGroup}/?token=${token}&languageCode=${lang}`)
        cy.on('uncaught:exception', (err, runnable) => {
            return false
        })

        cy.get('#main_middle .main_nav_top_right .top_nav_options_text').then(($OddsType_Sev) => {
            OddsType_Sev = $OddsType_Sev
        })
    })


    it('bet slip is shown upon click on odds', function () {
        cy.viewport(windowWidth, windowHeight)
        cy.visit(`${host}/sev/${sportId}/${marketId}/${eventId}/${includeEventGroup}/?token=${token}&languageCode=${lang}`)
        cy.on('uncaught:exception', (err, runnable) => {
            return false
        })
        cy.get('#tabSoccerAll').then(($All) => {
            cy.wrap($All).click()
            cy.wait(1000)
        })


        cy.get('.bet_type_wrap > .bet_type_row > .left > div:nth-child(1)').each(($Period_Sev) => {
            cy.get('.bet_type_wrap > .bet_type_row > .left > div:nth-child(2)').each(($BetType_Sev) => {
                cy.get('#sev_main_wrap .bet_type_wrap .handi').each(($BetTypeSelection_Sev) => {
                    cy.get('#sev_main_wrap .bet_type_wrap .odds > div').each(($Odds_Sev) => {
                        // action
                        testOddsClick(OddsType_Sev, $Odds_Sev, $Period_Sev, $BetType_Sev, $BetTypeSelection_Sev)
                        
                        testInsertStake(OddsType_Sev, $Odds_Sev, $Period_Sev, $BetType_Sev, $BetTypeSelection_Sev)
                        
                    })
                })
            })
        })
    })
})